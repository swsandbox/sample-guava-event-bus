package io.swsb;

import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.Subscribe;

import java.util.concurrent.Executors;

/**
 * Created by swsb.
 */
public class Sandbox
{

    public static void main(String[] args) throws InterruptedException
    {
        AsyncEventBus eventBus = new AsyncEventBus(Executors.newFixedThreadPool(10));
        eventBus.register(new Greeter());
        eventBus.register(new MeanGreeter());

        eventBus.post("Jesse");
        eventBus.post(7);
        eventBus.post(1);

        Foo f = new Foo();
        f.setName("jesse");

        eventBus.post(f);


        System.out.println("waiting...");
    }

    private static class Greeter
    {
        @Subscribe
        public void sayHello(String name) throws InterruptedException
        {
            System.out.println("Hello " + name);
        }

        @Subscribe
        public void saySnarkyHello(String name)
        {
            System.out.println("Hello " + name + " ..snark");
        }
    }

    private static class MeanGreeter
    {
        int count = 0;

        @Subscribe
        public void sayMeanHello(String name)
        {
            System.out.println("meanie... " + name);
        }

        @Subscribe
        public void countIntegers(Integer n)
        {
            count = count + n;
            System.out.println("the count is: " + count);
        }

        @Subscribe
        public void hiFoo(Foo foo)
        {
            System.out.println(foo);
        }
    }

    private static class Foo
    {
        private String name;

        public String getName()
        {
            return name;
        }

        public void setName(String name)
        {
            this.name = name;
        }

        @Override
        public String toString()
        {
            return "Foo{" +
                    "name='" + name + '\'' +
                    '}';
        }
    }


}
